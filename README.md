Some Adblock Plus fiter lists

- boycott-postmedia: a list to block Post Media properties
  [Add to your ad blocker](abp:subscribe?location=https://gitlab.com/hfiguiere/hub-filters/raw/master/boycott-postmedia&title=Boycott%20Post%20Media)
  If this doesn't work ad this URL
  https://gitlab.com/hfiguiere/hub-filters/raw/master/boycott-postmedia

The filter lists are licensed under Creative Commons CC0